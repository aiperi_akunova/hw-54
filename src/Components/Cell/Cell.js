import React from 'react';
import './Cell.css';

const Cell = (props) => {

    const cellColor = {backgroundColor: 'green'}

    // for (let i = 0; i < props.cells.length; i++) {
    //     if(props.cells[i].open === true){
    //         cellColor.backgroundColor='white';
    //     }
    // }


    return (
        <div className='cell' style={cellColor} onClick={(e)=>props.onClick(e)} id = {props.id} >
        </div>
    );
};

export default Cell;