import './App.css';
import {useState} from "react";
import Cell from "./Components/Cell/Cell";

const App = () => {

    const cells = [];
    for (let i = 0; i < 36; i++) {
        cells.push({open: false, hasItem: false, id: i});
    }

    const indexToHide = Math.floor(Math.random()*36)+1;

    // console.log(indexToHide)
     const hiddenObj = cells[indexToHide].hasItem = true;
    // console.log(hiddenObj)

    const [AllCells, setAllCells] = useState(cells);

    // console.log(AllCells);

    // console.log(cells);



    const turnCell = (e) =>{
        setAllCells(AllCells.map(c=>{
            if(c.id === parseInt(e.target.id)){
                return {...c, open:true}
            }
            return c;
        }))
        console.log(e.target.id)

        console.log(AllCells);
    }

    const allCells = AllCells.map((c,index)=>(

        <Cell
            cells = {AllCells}
            id = {c.id}
            onClick = {turnCell}
            key = {index}
        />
    ));

    return (
        <div className="App">
            <div className='main-box'>
                {allCells}
            </div>

        </div>
    );
};

export default App;
